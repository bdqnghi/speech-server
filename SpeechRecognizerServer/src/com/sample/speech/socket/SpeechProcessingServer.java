package com.sample.speech.socket;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

import com.sample.speech.recognizer.WavFileRecognizerv3;

public class SpeechProcessingServer {
	private static WavFileRecognizerv3 recognizer = new WavFileRecognizerv3();
	public static void main(String[] args) throws IOException {
		System.out.println("start");
		
		while(true) {
		try {
			// Create a socket
			
			int PORT = 5555;
			ServerSocket srvr = new ServerSocket(PORT);
			
			Socket skt = srvr.accept();
			
			// Create a file output stream, and a buffered input stream
			String OUTPUTFILENAME = System.getProperty("java.io.tmpdir")
					+ "server.wav";
			File fs = new File(OUTPUTFILENAME);
			FileOutputStream fos = new FileOutputStream(OUTPUTFILENAME);
			BufferedOutputStream out = new BufferedOutputStream(fos);
			BufferedInputStream in = new BufferedInputStream(skt
					.getInputStream());

			
			
			// Read, and write the file to the socket
			int i;
			byte[] byt = new byte[8192];
			while ((i = in.read(byt)) != -1)
				out.write(byt, 0, i);
			
			out.flush();
			in.close();
			out.close();
			skt.close();
			srvr.close();
			sendScore(fs);	
			
		} catch (Exception e) {
			System.out.print("Error! It didn't work! " + e + "\n");
			e.printStackTrace();
		}
		}

	}// end main

	private static void sendScore(File fs) {
		try {
		
			ServerSocket srvr = new ServerSocket(5556);
			Socket skt = srvr.accept();
			
			PrintWriter writer = new PrintWriter(skt.getOutputStream(),true);
			String ret = recognizer.getScore(fs.toURI().toURL());
			
			writer.write(ret);
			writer.close();
			skt.close();
			srvr.close();
		}catch(Exception e){System.out.println("Error! " +e);
		e.printStackTrace();}
		
	}
	
}
