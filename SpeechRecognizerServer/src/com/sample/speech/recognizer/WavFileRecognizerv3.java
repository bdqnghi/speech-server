package com.sample.speech.recognizer;

import java.io.File;
import java.net.URL;
import java.text.DecimalFormat;

import javax.sound.sampled.AudioInputStream;

import edu.cmu.sphinx.frontend.util.AudioFileDataSource;
import edu.cmu.sphinx.recognizer.Recognizer;
import edu.cmu.sphinx.result.ConfidenceResult;
import edu.cmu.sphinx.result.ConfidenceScorer;
import edu.cmu.sphinx.result.Path;
import edu.cmu.sphinx.result.Result;
import edu.cmu.sphinx.util.props.ConfigurationManager;

public class WavFileRecognizerv3 {

	private URL audioFileURL;
	private URL configURL;
	private ConfigurationManager cm;
	private Recognizer recognizer;
	private AudioFileDataSource dataSource;

	public WavFileRecognizerv3() {
		intialize();
	}

	public WavFileRecognizerv3(File audioFile) {
		try {
			intialize();
			audioFileURL = audioFile.toURI().toURL();
		} catch (Exception e) {
			System.out.println("File not Initialized !!!");
		}
	}

	private void intialize() {
		configURL = WavFileRecognizerv3.class
				.getResource("trigram.config.xml");
		cm = new ConfigurationManager(configURL);
		recognizer = (Recognizer) cm.lookup("recognizer");
		recognizer.allocate();
		dataSource = (AudioFileDataSource) cm.lookup("audioFileDataSource");

	}

	public String getScore(AudioInputStream aiStream) {
		setAudioInputStream(aiStream);
		return calculateScore();
	}

	private void setAudioInputStream(AudioInputStream aiStream) {
		dataSource.setInputStream(aiStream, null);
	}

	// Using URL to access Audio File
	public String getScore(URL audioFileUrl) {
		setAudioFileUrl(audioFileUrl);
		return calculateScore();
	}

	private void setAudioFileUrl(URL audioFile) {
		try {
			audioFileURL = audioFile;
			dataSource.setAudioFile(audioFileURL, null);
		} catch (Exception e) {
		}
	}

	private String calculateScore() {
		Result result = recognizer.recognize();
		if (result == null) {
			return "What did you Speak??";
		}

		ConfidenceScorer cs = (ConfidenceScorer) cm.lookup("confidenceScorer");
		
		ConfidenceResult cr = cs.score(result);
		Path best = cr.getBestHypothesis();
		
		DecimalFormat format = new DecimalFormat("#.#####");
	    

		String score = format.format(best.getLogMath().logToLinear
                ((float) best.getConfidence()));
         /*
         * print out confidence of individual words
         * in the best path
         */
        /* WordResult[] words1 = best.getWords();
         for (int i = 0; i < words1.length; i++) {
             WordResult wr = words1[i];
             printWordConfidence(wr);
         }*/

		String words = result.getBestFinalResultNoFiller();
		
		if (words == null || words.isEmpty())
			return "What did you Speak??";

		return "Word Recognized: " + words + " | Confidence: " + score;
	}

	
	/*private static void printWordConfidence(WordResult wr) {
        String word = wr.getPronunciation().getWord().getSpelling();

        System.out.print(word);

         pad spaces between the word and its score 
        int entirePadLength = 10;
        if (word.length() < entirePadLength) {
            for (int i = word.length(); i < entirePadLength; i++) {
                System.out.print(" ");
            }
        }

        DecimalFormat format = new DecimalFormat("#.#####");
        System.out.println
                (" (confidence: " +
                        format.format
                                (wr.getLogMath().logToLinear((float) wr.getConfidence())) + ")");
    }*/
}
